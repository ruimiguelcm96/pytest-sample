import pytest


def calculate_sum(a: int, b: int) -> int:
    return a + b


@pytest.mark.parametrize(
    "a, b, response",
    [
        (1, 2, 3), (0, 3, 3), (0, -7, -7)
    ]
)
def test_calculate_sum(a, b, response):
    assert calculate_sum(1, 2) == 3
